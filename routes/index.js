var express = require('express');
var router = express.Router();
const moment= require('moment') 


/* GET home page. */
router.get('/', function(req, res, next) {
  const url = 'https://api.nasa.gov/planetary/earth/imagery';
  const apikey = 'w1U19fmVGEAlzfjVigDJ4fONBQk8hb8QKmNYxC1m';
  let lat = req.query.lat * 1;
  let lon = req.query.lon * 1;
  let info = req.query.info;
  let today = moment(new Date()).format("YYYY-MM-DD");
  let imgbase = [];
  if(lat !== undefined && lat !== 0 && lon !== undefined && lon !== 0) {
    imgbase[0] = url + '?lat=' + (lat - 0.05) + '&lon=' + (lon - 0.05) + '&date=' + info + '&dim=0.05&api_key=' + apikey;
    imgbase[1] = url + '?lat=' + (lat - 0.05) + '&lon=' + lon + '&date=' + info + '&dim=0.05&api_key=' + apikey;
    imgbase[2] = url + '?lat=' + (lat - 0.05) + '&lon=' + (lon + 0.05) + '&date=' + info + '&dim=0.05&api_key=' + apikey;
    imgbase[3] = url + '?lat=' + lat + '&lon=' + (lon - 0.05) + '&date=' + info + '&dim=0.05&api_key=' + apikey;
    imgbase[4] = url + '?lat=' + lat + '&lon=' + lon + '&date=' + info + '&dim=0.05&api_key=' + apikey;
    imgbase[5] = url + '?lat=' + lat + '&lon=' + (lon + 0.05) + '&date=' + info + '&dim=0.05&api_key=' + apikey;
    imgbase[6] = url + '?lat=' + (lat + 0.05) + '&lon=' + (lon - 0.05) + '&date=' + info + '&dim=0.05&api_key=' + apikey;
    imgbase[7] = url + '?lat=' + (lat + 0.05) + '&lon=' + lon + '&date=' + info + '&dim=0.05&api_key=' + apikey;
    imgbase[8] = url + '?lat=' + (lat + 0.05) + '&lon=' + (lon + 0.05) + '&date=' + info + '&dim=0.05&api_key=' + apikey;
    today = moment(req.query.info).format("YYYY-MM-DD");
  }   
  res.render('index', { title: 'SpaceApp', imgsrc: imgbase, defdate: today });
});

module.exports = router;

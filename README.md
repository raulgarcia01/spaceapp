## _SpaceApp RSM Dev Node JS Testing_

NodeJS consuming NASA Earth Images API


## Installation instructions:

- Clone this repository and save it in your computer
- Be sure to be located inside the root folder of this project, Example cd spaceapp/
- Execute **npm install**
- Run the project using **npm start**, and enjoy searching satellite Earth images :)